<?php

namespace App\Exceptions;

use Exception;

class MatrixIsSingular extends Exception
{
    protected $message = 'Matrix is singular!';
}
