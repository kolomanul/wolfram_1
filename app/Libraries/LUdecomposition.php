<?php
namespace App\Libraries;

use Phpml\Math\Matrix;
use App\Exceptions\MatrixNotSquare;
use Illuminate\Support\Facades\Session;

class LUdecomposition
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $matrox_obj;
    protected $b;

    public function __construct(Matrix $matrix)
    {
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
        $this->matrix_obj = $matrix;
        $this->matrix = $this->getMatrices($matrix->toArray());
    }

    protected function getMatrices($matrix) 
    {
        if ($this->columns - $this->rows == 1) {
            for ($i = 0; $i < $this->rows; $i++){
                for ($j = 0; $j < $this->columns-1; $j++) {
                    if ($j == $this->columns - 2)
                        $this->b[$i] = $matrix[$i][$this->columns-1];
                    $var[$i][$j] = $matrix[$i][$j];
                }
            }
            return $var;
        }
        return $matrix;
    }

    public function handle()
    {
        $this->getLowerAndUpperMatrices($this->matrix);
        return $this->solution;
    }

    protected function getLowerAndUpperMatrices(array $matrix) {
        $mat = $matrix;
        $n = $this->rows;

        for($i = 0; $i < $n; $i++) 
            for($j = 0; $j < $n; $j++) { 
                $lower[$i][$j]= 0; 
                $upper[$i][$j]= 0; 
            } 

        for ($i = 0; $i < $n; $i++) 
        { 
            // Upper Triangular 
            for ($k = $i; $k < $n; $k++) 
            { 
                $sum = 0; 
                for ($j = 0; $j < $i; $j++) 
                    $sum += ($lower[$i][$j] * $upper[$j][$k]); 

                $upper[$i][$k] = $mat[$i][$k] - $sum; 
            } 

            // Lower Triangular 
            for ($k = $i; $k < $n; $k++) 
            { 
                if ($i == $k) 
                    $lower[$i][$i] = 1; // Diagonal as 1 
                else
                { 
                    // Summation of L(k, j) * U(j, i) 
                    $sum = 0; 
                    for ($j = 0; $j < $i; $j++) 
                        $sum += ($lower[$k][$j] * $upper[$j][$i]); 

                    $lower[$k][$i] = (($mat[$k][$i] - $sum) / $upper[$i][$i]); 
                } 
            } 
        } 

        for($i=0; $i<$n; $i++)
        {
            $Y[$i] = $this->b[$i];
            for($j = 0; $j < $i; $j++) {
                $Y[$i]-=$lower[$i][$j]*$Y[$j];
            }
        }
        
        for($i= $n-1 ; $i >= 0; $i--) {
            $X[$i] = $Y[$i];
            for($j = $i+1; $j < $n; $j++) {
                $X[$i] -= $upper[$i][$j]*$X[$j];
            }
            $X[$i] /= $upper[$i][$i];
        }

        $this->solution = [
            'input'        => $this->matrix,
            'Lower_Matrix' => $this->round($lower, $this->rows, $this->rows),
            'Upper_Matrix' => $this->round($upper, $this->rows, $this->rows), 
            'X'            => $X,
            'Y'            => $Y
        ];

    }

    protected function checkForSquareMatrix(Matrix $matrix)
    {
        if ($this->rows !== $this->columns){
            return 1;
        }
    }

    protected function round(array $matrix, int $n, int $m)
    {
        for ($i = 0; $i < $n; $i++)
            for ($j = 0; $j < $m; $j++)
                $matrix[$i][$j] = round($matrix[$i][$j], 2);
        return $matrix;
    }
}