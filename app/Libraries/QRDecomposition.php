<?php
namespace App\Libraries;

use Phpml\Math\Matrix;
use App\Exceptions\MatrixNotSquare;

class QRDecomposition
{
    public function __construct(Matrix $matrix)
    {
        $this->matrix_obj = $matrix;
        $this->matrix = $matrix->toArray();
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
    }

    public function handle()
    {
        $this->checkForSquareMatrix($this->matrix_obj);
        $this->houseHolder($this->matrix);
        return $this->solution;
    }

    protected function checkForSquareMatrix(Matrix $matrix)
    {
        if ($this->rows !== $this->columns)
            throw new MatrixNotSquare();
    }

    protected function houseHolder(array $matrix)
    {
        dd('ezt beszoptam');
    }
}