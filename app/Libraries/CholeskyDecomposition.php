<?php
namespace App\Libraries;

use Phpml\Math\Matrix;
use App\Exceptions\MatrixNotSquare;
use Phpml\Math\LinearAlgebra\LUDecomposition;

class CholeskyDecomposition
{
    protected $matrix;
    protected $rows;
    protected $columns;
    protected $solution;
    protected $matrox_obj;
    protected $b;

    public function __construct(Matrix $matrix)
    {
        $this->rows = $matrix->getRows();
        $this->columns = $matrix->getColumns();
        $this->matrix_obj = $matrix;
        $this->matrix = $this->getMatrices($matrix->toArray());
    }

    protected function getMatrices($matrix) 
    {
        if ($this->columns - $this->rows == 1) {
            for ($i = 0; $i < $this->rows; $i++){
                for ($j = 0; $j < $this->columns-1; $j++) {
                    if ($j == $this->columns - 2)
                        $this->b[$i] = $matrix[$i][$this->columns-1];
                    $var[$i][$j] = $matrix[$i][$j];
                }
            }
            return $var;
        }
        return $matrix;
    }

    public function handle()
    {
        // $this->checkForSquareMatrix($this->matrix_obj);
        $this->choleskyDecomposition($this->matrix);
        return $this->solution;
    }

    protected function checkForSquareMatrix(Matrix $matrix)
    {
        if ($this->rows !== $this->columns)
            throw new MatrixNotSquare();
    }

    protected function choleskyDecomposition(array $matrix)
    {
        for ($i = 0; $i < $this->rows; $i++) {
            $z[$i] = 0;
            $x[$i] = 0;
            for ($j = 0; $j < $this->rows; $j++) 
                $Lower[$i][$j] = 0; 
        }

        for ($i = 0; $i < $this->rows; $i++)  
        { 
            for ($j = 0; $j <= $i; $j++)  
            { 
                $sum = 0; 
    
                if ($j == $i)  
                { 
                    for ($k = 0; $k < $j; $k++) 
                        $sum += pow($Lower[$j][$k], 2); 
                        // dump($matrix[$j][$j] - $sum);
                        //check if subtraction is negative. if it is, throw an error
                    $Lower[$j][$j] = sqrt($matrix[$j][$j] - $sum); 
                } 
                else 
                { 
                    for ($k = 0; $k < $j; $k++) 
                        $sum += ($Lower[$i][$k] * $Lower[$j][$k]); 

                    $Lower[$i][$j] = ($matrix[$i][$j] - $sum) / $Lower[$j][$j]; 
                } 
            } 
        } 

        for ($i = 0; $i < $this->rows; $i++)
            for ($j = 0; $j < $this->rows; $j++)
                $Upper[$i][$j] = $Lower[$j][$i];

        for ($i = 0; $i < $this->rows; $i++)
        {
            $sum=0;
            for($p = 0; $p < $i; $p++) {
                $sum += $Lower[$i][$p] * $z[$p];
            }
            $z[$i] = ($this->b[$i] - $sum) / $Lower[$i][$i];
        }
        //********** FINDING X; UX=Z***********//
    
        for($i = $this->rows-1; $i >= 0; $i--)
        {
            $sum=0;
            for($p = $this->rows - 1; $p > $i-1; $p--)
                $sum += $Upper[$i][$p] * $x[$p];
            $x[$i] = round(($z[$i]-$sum) / $Upper[$i][$i], 2);
        }
        
        $this->solution = [
            'input'        => $this->matrix_obj->toArray(),
            'Lower_Matrix' => $this->round($Lower, $this->rows, $this->rows),
            'Upper_Matrix' => $this->round($Upper, $this->rows, $this->rows),
            'X'            => $x,
            'Y'            => $z
        ];
    }

    protected function round(array $matrix, int $n, int $m)
    {
        for ($i = 0; $i < $n; $i++)
            for ($j = 0; $j < $m; $j++)
                $matrix[$i][$j] = round($matrix[$i][$j], 2);
        return $matrix;
    }
}