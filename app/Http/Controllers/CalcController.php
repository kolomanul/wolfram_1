<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Libraries\GaussElimination;
use Phpml\Math\Matrix;
use App\Libraries\GaussJordan;
use App\Libraries\LUPdecomposition;
use App\Libraries\LUdecomposition;
use App\Libraries\CholeskyDecomposition;

class CalcController extends Controller
{
    public function cholesky(Request $request)
    {
        if ($request->hasFile('file')) {
            $matrix = InputController::getFileInput($request->file('file'));
        } else {
            if ($request->matrix == null){
                Session::flash('error', 'No file uploaded!');
                return back();
            }
            $matrix = InputController::getInput($request->matrix);
        }
        try{
            $solution = (new CholeskyDecomposition($matrix))->handle();
            return view('solutions.cholesky_solution')->with('data', $solution);
        } catch(\Exception $e) {
            Session::flash("error", "Something went wrong!");
            return view('cholesky');
        }
    }
    
    public function gaussElimination(Request $request)
    {
        if ($request->hasFile('file')) {
            $matrix = InputController::getFileInput($request->file('file'));
        } else {
            if ($request->matrix == null){
                Session::flash('error', 'No file uploaded!');
                return back();
            }
            $matrix = InputController::getInput($request->matrix);
        }
        try {
            $solution = (new GaussElimination($matrix))->handle();
            return view('solutions.gauss_solutions')->with('data', $solution);
        } catch(\Exception $e) {
            Session::flash("error", "Something went wrong!");
            return view('gauss');
        }
    }

    public function gaussJordan(Request $request)
    {
        if ($request->hasFile('file')) {
            $matrix = InputController::getFileInput($request->file('file'));
        } else {
            if ($request->matrix == null){
                Session::flash('error', 'No file uploaded!');
                return back();
            }
            $matrix = InputController::getInput($request->matrix);
        }
        try {
            $solution = (new GaussJordan($matrix))->handle();
            return view('solutions.gauss_jordan_solution')->with('data', $solution);
        } catch(\Exception $e) {
            Session::flash("error", "Something went wrong!");
            return view('gauss_jordan');
        }
    }

    public function luDecomposition(Request $request)
    {
        if ($request->hasFile('file')) {
            $matrix = InputController::getFileInput($request->file('file'));
        } else {
            if ($request->matrix == null){
                Session::flash('error', 'No file uploaded!');
                return back();
            }
            $matrix = InputController::getInput($request->matrix);
        }
        try{
            $solution = (new LUDecomposition($matrix))->handle();
            return view('solutions.lu_solution')->with('data', $solution);
        } catch(\Exception $e) {
            Session::flash("error", "Something went wrong!");
            return view('lu_decomposition');
        }
    }

    public function lupDecomposition(Request $request)
    {
        if ($request->hasFile('file')) {
            $matrix = InputController::getFileInput($request->file('file'));
        } else {
            if ($request->matrix == null){
                Session::flash('error', 'No file uploaded!');
                return back(); 
            }
            $matrix = InputController::getInput($request->matrix);
        }
        try{
            $solution = (new LUPDecomposition($matrix))->handle();
            return view('solutions.lup_solution')->with('data', $solution);
        } catch(\Exception $e) {
            Session::flash("error", "Something went wrong!");
            return view('lup_decomposition');
        }
    }
}
