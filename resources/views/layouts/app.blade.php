<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Wolfram</title>
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <nav class="site-header py-1 header mb-3">
        <div class="ml-3 d-flex flex-column justify-content-between">
            <div class="row">
                <div class="col-sm-1">
                    <a class="py-2" href="/">
                        Wolfram
                    </a>
                </div>
                <div class="col-sm-1">
                    <a class="py-2" href="https://gitlab.com/kulcsarkalman1996/wolfram_1">
                        GitLab
                    </a>
                </div>
                <div class="col-sm-1">
                    <a class="py-2" href="https://gitlab.com/kulcsarkalman1996/wolfram_1">
                        overleaf
                    </a>
                </div>
            </div>
        </div>
    </nav>
    <div class="row">
        <div class="col-2">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="/gauss-elim">Gauss Elimination</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gauss-jordan">Gauss Jordan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="lu-decomposition">LU decomposition</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="lup-decomposition">LUP Decomposition</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active " href="/cholesky">Cholesky Decomposition</a>
                </li>
            </ul>
        </div>
        <div class="col-10">
            @if (Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    {{Session::get('error')}}
                </div>
            @endif

            @yield('content')
        </div>
    </div>

    <script data-main="/js/app" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
