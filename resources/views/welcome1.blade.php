
@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Wolfram
            </div>
            <div class="links">
                <a href="https://laravel.com/docs">Docs</a>
                <a href="https://gitlab.com/kulcsarkalman1996/wolfram_1">GitHub</a>
            </div>
            <div class="pt-5">
                {{ Form::open(array('url' => '/det','files'=>'true'))}}
                {{'Select the file to upload'}}
                {{ Form::file('file')}}
                <br>
                {{ Form::submit('Upload File')}}
                {{ Form::close()}}
            </div>
        </div>
    </div>
@endsection
