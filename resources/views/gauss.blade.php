
@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Gauss elmination
            </h2>
            <form method="POST" action="{{route('gauss-elim.calc')}}">
                @csrf
                @component('layouts.manual')
                @endcomponent
            </form>
            
            <div class="mt-5">
                {{ Form::open(array('route' => 'gauss-elim.calc','files'=>'true'))}}
                    @csrf
                    @component('layouts.file') 
                    @endcomponent
                {{ Form::close()}}
            </div>
        </div>
        <div class="col-6">
            <br>
            <br>
            {{-- description goes here
            <br>
            <br>
            description goes here
            <br>
            <br>
            description goes here --}}
        </div>
    </div>
    
@endsection
