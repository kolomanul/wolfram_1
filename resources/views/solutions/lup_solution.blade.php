@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-6">
            <h2>
                LUP decomposition
            </h2>
            <form method="POST" action="{{route('lup-decomposition.calc')}}">
                @csrf
                @component('layouts.manual')
                @endcomponent
            </form>
            
            <div class="mt-5">
                {{ Form::open(array('route' => 'lup-decomposition.calc','files'=>'true'))}}
                    @csrf
                    @component('layouts.file') 
                    @endcomponent
                {{ Form::close()}}
            </div>
        </div>
        <div class="col-6">
            <br>
            <br>
            {{-- description goes here
            <br>
            <br>
            description goes here
            <br>
            <br>
            description goes here --}}
        </div>
    </div>

<div id="solution">
    <div class="row mt-4">
            <div class="col-sm-2">Input matrix: </div>
            <div class="col-sm-2">
                <table>
                @foreach ($data['input'] as $key =>  $row)
                    <tr>
                    @foreach ($row as $item)
                        <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                    @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm-2">L*U matrix</div>
        <div class="col-sm-2">
            <table>
                @foreach ($data['LU'] as $key =>  $row)
                    <tr>
                    @foreach ($row as $item)
                        <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                    @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-2">Solutions: </div>
                <div class="col-sm-2" id="solution">
                    @foreach ($data['X'] as $key =>  $row)
                        {{'x_'. $key . ' = ' . $row }}
                        <br>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
  
    <div class="row mt-4">
        <div class="col-sm-2">P matrix: </div>
        <div class="col-sm-2">
            <table>
            @foreach ($data['P'] as $row)
                <tr>
                @foreach ($row as $item)
                    <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                @endforeach
                </tr>
            @endforeach
            </table>
        </div>
    </div>
</div>

@endsection
