@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-6">
            <h2>
                Gauss-Jordan method
            </h2>
            <form method="POST" action="{{route('gauss-elim.calc')}}">
                @csrf
                @component('layouts.manual')
                @endcomponent
            </form>
            
            <div class="mt-5">
                {{ Form::open(array('route' => 'gauss-elim.calc','files'=>'true'))}}
                    @component('layouts.file') 
                    @endcomponent
                {{ Form::close()}}
            </div>
            <div class="row mt-5">
                <div class="col-sm-2 mt-4">Input matrix: </div>
                    <div class="col-sm-2">
                        <table>
                        @foreach ($data['input'] as $key =>  $row)
                            <tr>
                            @foreach ($row as $item)
                                <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                            @endforeach
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            
            <div id="solution">
                @foreach ($data['solution'] as $key =>  $row)
                    {{'x_'. $key . ' = ' . $row }}
                    <br>
                @endforeach
            </div>
        </div>
        <div class="col-6">
            <br>
            <br>
            {{-- description goes here
            <br>
            <br>
            description goes here
            <br>
            <br>
            description goes here --}}
        </div>
    </div>
@endsection
