@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col-6">
            <h2>
                LU decomposition
            </h2>
            <form method="POST" action="{{route('lu-decomposition.calc')}}">
                @csrf
                @component('layouts.manual')
                @endcomponent
            </form>
            
            <div class="mt-5">
                @csrf
                {{ Form::open(array('route' => 'lu-decomposition.calc','files'=>'true'))}}
                    @component('layouts.file') 
                    @endcomponent
                {{ Form::close()}}
            </div>
        </div>
        <div class="col-6">
            <br>
            <br>
            {{-- description goes here
            <br>
            <br>
            description goes here
            <br>
            <br>
            description goes here --}}
        </div>
    </div>
    

<div id="solution">
    <div class="row mt-4">
        <div class="col-sm-2">Input matrix: </div>
        <div class="col-sm-2">
            <table>
            @foreach ($data['input'] as $key =>  $row)
                <tr>
                @foreach ($row as $item)
                    <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                @endforeach
                </tr>
            @endforeach
            </table>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm-2">Lower triangular matrix: </div>
        <div class="col-sm-2">
            <table>
            @foreach ($data['Lower_Matrix'] as $key =>  $row)
                <tr>
                @foreach ($row as $item)
                    <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                @endforeach
                </tr>
            @endforeach
            </table>
        </div>
        <div class="col-sm-4">
            <div class="row">
                <div class="col-sm-2">Solutions: </div>
                <div class="col-sm-2" id="solution">
                    @foreach ($data['X'] as $key =>  $row)
                        {{'x_'. $key . ' = ' . $row }}
                        <br>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
            <div class="col-sm-2">Upper triangular matrix: </div>
            <div class="col-sm-2">
                <table>
                @foreach ($data['Upper_Matrix'] as $key =>  $row)
                    <tr>
                    @foreach ($row as $item)
                        <td class="matrix-input" align="center">{{' ' . $item . ' '}}</td>
                    @endforeach
                    </tr>
                @endforeach
                </table>
            </div>
        </div>
</div>

@endsection
